#pragma once
#define TEST
// Qt
#include <QX11Info>
#include <QVector>
#include <QMatrix4x4>
// Qt Quick
#include <QQuickItem>
#include <QQuickWindow>
// Qt OpenGL
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>
// xcb
#include <xcb/composite.h>

class OpenGLRender;
class WindowPreview : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(uint winId READ winId WRITE setWinId NOTIFY winIdChanged)
    
public:
    explicit WindowPreview(QQuickItem* parent = nullptr);

    uint32_t winId() const;
    void setWinId(uint32_t winId);

Q_SIGNALS:
    void winIdChanged();

protected slots:
    void Render();
    void OnWindowChanged(QQuickWindow* w);
    void Release();

protected:
    uint32_t m_winId;
    xcb_connection_t* c;
    QVector<GLfloat> data;
    QOpenGLBuffer VBO;
    QOpenGLShaderProgram program;
    QOpenGLTexture *texture = nullptr;
    QImage image_data;
    QColor background;
    
    bool InitGL();
    void setImage(QImage &image);
    void updateWindowPreview(); // 刷新窗口预览图像
    // test only
    void setImage(const QString &filename);
};
